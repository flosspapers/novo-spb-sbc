# Software Público Brasileiro: de portal para plataforma integrada de colaboração

O Governo Federal vem nos últimos anos melhorando seus processos de
desenvolvimento e adoção de software. Em 2003, a recomendação da adoção de
software livre passou a ser uma política, incentivada com a criação do _Guia
Livre_. Em 2007, a Secretaria de Logística e Tecnologia da Informação (SLTI) do
Ministério do Planejamento, Orçamento e Gestão (MP) lançou o portal do
_Software Público Brasileiro_ (SPB) -- um sistema web para o compartilhamento
de projetos de software no Governo.

Por um lado, a _Instrução Normativa 04/2012_ indica que os gestores devem
consultar as soluções existentes no portal do SPB antes de realizar uma
contratação de software. Por outro, a evolução técnica do portal do SPB foi
comprometida, desde 2009, ao não acompanhar a evolução do seu _framework_ base,
o _OpenACS_. Não houve o lançamento de novas versões do portal desde então.

Uma nova plataforma para o SPB está sendo desenvolvida pela Universidade de
Brasília, através dos seus Laboratórios LAPPIS e MídiaLab em parceria com o
Centro de Competência em Software Livre da Universidade de São Paulo
(CCSL-USP). A nova plataforma é baseada na integração de ambientes
colaborativos, sistemas de controle de versão e de monitoramento da qualidade
do código-fonte, e está sendo desenvolvida por uma equipe heterogênea composta
por alunos, professores e profissionais, aplicando métodos ágeis e práticas de
desenvolvimento distribuído de software.

<!------------------------------------------------------------------------>

**Evolução para uma plataforma integrada de colaboração**

O conceito de software público se diferencia do de software livre em alguns
aspectos, destacando-se a atribuição de bem público ao software. Isso significa
que o Governo, especificamente o MP, assume algumas responsabilidades que
garantem ao usuário do software, em especial os órgãos públicos, condições
adequadas de uso. Embora haja diferenças entre o que é um software
livre e um software público brasileiro, há princípios comuns como a tendência
da descentralização na tomada de decisões, do compartilhamento de
informações e da retroalimentação. Por isso, a nova plataforma
para o SPB foi pensada para contemplar ferramentas que promovam a colaboração e
a interação nas comunidades (por gestores, usuários e desenvolvedores) dos
projetos, conforme as práticas usadas nas comunidades de software livre. Isso
inclui listas de e-mail, fóruns de discussão, _issue trackers_, sistemas de
controle de versão e ambientes de rede social.

Para integrar as ferramentas e prover a autenticação única nos serviços da
plataforma, um sistema web chamado Colab, que funcionada como _proxy reverso_
para os ambientes, está sendo evoluído. Em resumo, o Colab oferece a integração
de busca, autenticação e apresentação, provendo um único ambiente ao usuário
que tem em seu perfil algumas métricas de contribuições (e-mails para listas,
inserções em _wikis_, cadastros de _issue_ e _commits_ nos repositórios).

O Colab foi desenvolvido para o Interlegis (programa do Senado Federal). Por
padrão, funciona integrado com o servidor de listas de e-mail _GNU Mailman_ e
utiliza o _Apache Lucene Solr_ para a indexação dos conteúdos para as buscas.
A partir de 2014, as ferramentas GitLab e Noosfero foram integradas ao Colab
para compor o novo SPB.

O GitLab é uma plataforma de desenvolvimento colaborativo social integrada ao
sistema de controle de versão Git. É o ambiente mais técnico: os repositórios
dos projetos do SPB, com páginas _wiki_, _issue tracker_ e mecanismos de
controle de versão de código estão nele. O Noosfero é uma plataforma para rede
social e de economia solidária que contém funcionalidades de gerenciamento de
conteúdos (CMS), além de permitir a configuração das páginas de usuários e de
comunidades de forma flexível. É o ambiente de maior interação com o usuário do
SPB, desde os cadastros até o acesso às páginas dos projetos para _download_,
leitura de documentação e contato com os responsáveis.

A integração dessas ferramentas não está totalmente completa, pois demanda a
solução de questões complexas de arquitetura de software. O que foi
desenvolvido em 2014 está funcional e já supera o antigo portal do SPB em
muitos aspectos. Em 2015, os perfis das diferentes ferramentas estão sendo
integrados de modo que o usuário o gerencie em um único lugar. Os controles de
acesso e a gestão de permissões também estão evoluindo. O mecanismo de coleta
de dados e busca está sendo refatorado para acessar os conteúdos das novas
ferramentas integradas ao Colab. Além disso, o Mezuro, um sistema para o
monitoramento de métricas de código-fonte, está sendo acoplado ao Colab para
fornecer acompanhamento da qualidade do código dos projetos.

<!------------------------------------------------------------------------>

**Evolução da experiência do usuário**

A integração dos ambientes colaborativos vai além dos aspectos funcionais.
Oferecer à população uma experiência unificada desses ambientes é fundamental
para estimular o uso da plataforma, uma vez que reduz a percepção de
complexidade.

Assim, a arquitetura da informação está sendo redesenhada para proporcionar uma
navegação transparente e que atenda aos diversos tipos de usuário. Os modelos
de interação de cada ferramenta estão sendo harmonizados, diminuindo a curva de
aprendizado. Ao mesmo tempo, um novo estilo visual está sendo criado para
apresentar essa experiência unificada e para atender as diretrizes de
Identidade Padrão de Comunicação Digital do Governo Federal.

Os usuários fazem parte do processo. Em 2014, foi aplicado um questionário para
avaliar a satisfação das pessoas com o portal antigo e identificar problemas de
experiência do usuário. Em 2015, estão previstas pelo menos 4 atividades de
validação da nova plataforma com usuários e cidadãos interessados.

<!------------------------------------------------------------------------>

**Considerações finais**

A nova plataforma do SPB foi lançada para homologação em dezembro de 2014 e
está em uso por algumas comunidades em `beta.softwarepublico.gov.br`. Todas as
ferramentas são software livre e o que está sendo desenvolvido pelas equipes da
UnB e USP é publicado em repositórios abertos, disponíveis na versão beta do
próprio SPB. Mais importante do que isso, as melhorias necessárias nas
ferramentas utilizadas estão sendo contribuídas de volta para as respectivas
comunidades. Isso não só é o certo a se fazer do ponto de vista da comunidade
de software livre, como vai possibilitar a redução de custos de manutenção no
futuro para os cofres públicos e a evolução continuada da plataforma em
sinergia com outras organizações que fazem uso das mesmas ferramentas.

Disponibilizar um conjunto de ferramentas e melhorar a experiência do usuário
no ambiente é parte desse processo de reformulação do SPB. Aspectos culturais
da colaboração em rede para um efetivo uso do o que é fornecido na plataforma
necessitam ser amadurecidos pelo MP junto às comunidades do SPB. Além disso, a
demanda por maior impacto do software público na oferta de software, na adoção
das soluções disponibilizadas e na atração de colaboradores e usuários requer
intervenção. Um estudo para propostas de licenciamento e seus impactos para o
SPB, bem como para sanar as contradições presentes na _Instrução Normativa
01/2011_ (que dispõe sobre os procedimentos do SPB), também está sendo
conduzido pela UnB para complementar o que está sendo desenvolvido do ponto de
vista tecnológico.

