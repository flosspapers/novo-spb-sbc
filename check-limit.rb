#!/usr/bin/ruby

require 'yaml'

input = ARGV[0]
limits = YAML.load_file(ARGV[1])
limit = limits[input]

size = `pandoc --standalone --from markdown --to plain #{input} | sed '/^[=-]\\+$/d' | wc --chars`.split.first.to_i

if size <= limit
  puts "%s: inside character limit (%d/%d)" % [input, size, limit]
else
  puts "%s: beyond character limit (%d/%d)" % [input, size, limit]
  if ENV['CHECK']
    exit 1
  end
end
