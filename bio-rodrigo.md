Rodrigo Maia é consultor sênior em Design de Experiência de Usuário e bolsista
do Laboratório Avançado de Produção, Pesquisa e Inovação em Software da
Universidade de Brasília (LAPPIS/UNB), onde lidera as atividades de Design no
projeto de reformulação do Portal do Software Público Brasileiro. Possui
especialização em Tecnologias Digitais na Prática do Design e da Arte, pela
Concordia University de Montreal/Canadá (2012). 
