Antonio Terceiro é sócio fundador da Cooperativa de Tecnologias Livre (COLIVRE)
e bolsista do Laboratório Avançado de Produção, Pesquisa e Inovação em Software
da Universidade de Brasília (LAPPIS/UNB), atuando como líder técnico no projeto
de reformulação do Portal do Software Público Brasileiro. Possui Doutorado em
Ciência da Computação pela Universidade Federal da Bahia (2012). É membro do
projeto Debian e um dos principais desenvolvedores do projeto Noosfero.
