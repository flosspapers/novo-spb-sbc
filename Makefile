INPUT = $(shell cut -d : -f 1 limits.yaml)
DOCS = $(patsubst %.md, %.rtf, $(INPUT))
PDF = $(patsubst %.md, %.pdf, $(INPUT))
CHAR_LIMIT = 6500

all: $(DOCS)

pdf: $(PDF)

.PHONY: check

check: clean
	$(MAKE) CHECK=1

$(PDF): %.pdf: %.md
	ruby check-limit.rb $< limits.yaml
	pandoc --standalone --from markdown --to latex --output $@ $<

$(DOCS): %.rtf: %.md
	ruby check-limit.rb $< limits.yaml
	pandoc --standalone --from markdown --to rtf --output $@ $<

clean:
	$(RM) $(DOCS) $(PDF)
